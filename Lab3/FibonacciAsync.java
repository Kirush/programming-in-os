import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FibonacciAsync {
    private static final Logger logger = Logger.getLogger(FibonacciAsync.class.getName());

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        int n = 10;
        int result = GetNthFibonacciNumber(n);

        logger.log(Level.INFO, "Fibonacci number for n = {0} is {1}", new Object[]{n, result});
    }

    private static int GetNthFibonacciNumber(int n) throws IllegalArgumentException, ExecutionException, InterruptedException {
        if(n <= 0)
            throw new IllegalArgumentException("N should be more than 0");

        logger.log(Level.INFO, "Calculating Fibonacci number asynchronously for n = {0}", n);

        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> fibonacci(n));
        int result;
        try
        {
            result = future.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            logger.log(Level.SEVERE, "Error occurred while calculating Fibonacci number", e);
            throw e;
        }

        return result;
    }

    private static int fibonacci(int n) {
        if (n <= 1) {
            return n;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}