import java.io.File;

public class DirectoryLister {
    private static final String lineBeginningIndicator = "->";
    private static final String indentation = "\t";


    public static void listCurrentDirectory()
    {
        String directory = System.getProperty("user.dir");
        System.out.printf("current dir = %s%n", directory);

        File file = new File(directory);

        listFileOrDirectory(file,lineBeginningIndicator);
    }

    private static void listFileOrDirectory(File fileToList, String separator)
    {
        File[] files = fileToList.listFiles();

        if(files == null)
            return;

        for (File file : files)
        {
            printFile(separator,file);

            if (!file.isDirectory())
                continue;

            printDirectory(separator, file);
        }
    }

    private static void printDirectory(String separator, File file)
    {
        listFileOrDirectory(file,indentation+separator);
    }

    private static void printFile(String separator, File value)
    {
        System.out.println(separator + value.getName());
    }
}