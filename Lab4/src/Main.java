
public class Main {
    public static void main(String[] args) {
        ArraysMultiplier multiplier = new ArraysMultiplier();

        System.out.println("Multiplying numbers synchronously with sleep 0: ");
        TimeMethod(() -> multiplier.Multiply(0));

        System.out.println("Multiplying numbers synchronously with sleep 1: ");
        TimeMethod(() -> multiplier.Multiply(1));

        System.out.println("Multiplying numbers in parallel with sleep 0: ");
        TimeMethod(() -> multiplier.MultiplyParallel(0));

        System.out.println("Multiplying numbers in parallel with sleep 1: ");
        TimeMethod(() -> multiplier.MultiplyParallel(1));
    }

    private static void TimeMethod(Runnable method){
        long time1 = System.currentTimeMillis();
        method.run();
        System.out.printf("It took : %s ms\n", System.currentTimeMillis() - time1);
    }
}
