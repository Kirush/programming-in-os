import java.util.Random;
import java.util.stream.IntStream;

public class ArraysMultiplier {

    private final int[] input1;
    private final int[] input2;

    private final int DefaultArraySize = 10000;
    private final int ArraySize;

    public ArraysMultiplier(int[] input1, int[] input2 ) {
        if(input1.length != input2.length)
            throw new IllegalArgumentException("Input arrays must be the same size");

        this.input1 = input1;
        this.input2 = input2;

        ArraySize = this.input1.length;
    }

    public ArraysMultiplier(){
        this.input1 = new int[DefaultArraySize];
        this.input2 = new int[DefaultArraySize];

        ArraySize = DefaultArraySize;

        FillInputsWithRandomData();
    }

    public int[] MultiplyParallel(int sleepTime){
        int[] result = new int[ArraySize];

        IntStream.range(0, ArraySize)
                .parallel()
                .forEach(i -> {
                    result[i] = input1[i] * input2[i];
                    Sleep(sleepTime);
                });

        return result;
    }

    public int[] Multiply(int sleepTime){

        int[] result = new int[ArraySize];

        for (int i = 0; i < ArraySize; i++) {
            result[i] = input1[i] * input2[i];
            Sleep(sleepTime);
        }
        return  result;
    }

    private void FillInputsWithRandomData() {
        Random rand = new Random();
        for (int i = 0; i < DefaultArraySize; i++) {
            this.input1[i] = rand.nextInt(101);
            this.input2[i] = rand.nextInt(101);
        }
    }

    private void Sleep(int sleepTime){

        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}